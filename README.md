﻿## 客户端数据验证框架

网页程序自然离不开数据的更新与保存，更新与保存前的数据验证我们称之为 **客户端数据验证** ，本框架提供了一种非常简单的客户端数据验证方法

通过简单的 html 样式名称或者属性名框架与相关验证方法进行关联，控件失去焦点或者点击验证按钮时触发客户端验证逻辑

## 在线演示
单页面演示：http://longbowenterprise.gitee.io/slidercaptcha/  

## 快速开始

### 组件依赖 jQuery bootstrap Validate

### CSS

```html
<link href="./lib/twitter-bootstrap/css/bootstrap.min.css">
```
将引入样式表的 &lt;link&gt; 标签复制并粘贴到 &lt;head&gt; 中，并放在所有其他样式表之前。

### JS

```html
<script src="./lib/jquery/jquery.min.js"></script>
<script src="./lib/twitter-bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="./lib/validate/jquery.validate.js"></script>
<script src="./lib/validate/localization/messages_zh.min.js"></script>
<script src="./dist/longbow.validate.js"></script>
```

将引入脚本的 &lt;script&gt; 标签复制并粘贴到 &lt;body&gt; 最后面。

## 用法

要验证的网页代码示例

```html
<div class="card">
    <div class="card-header">网站名称设置</div>
    <div class="card-body" data-toggle="LgbValidate" data-valid-button="[data-method='title']">
        <div class="form-group">
            <div class="input-group">
                <input type="text" class="form-control" id="sysName" placeholder="请输入网站标题，50字以内" value="网站标题" maxlength="50" data-valid="true" />
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="button" data-method="title">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
```

1. 设置验证作用域 `data-toggle="LgbValidate"`
2. 设置验证按钮 `data-valid-button="[data-method='title']"`
3. 设置需要验证的控件 `data-valid="true"`
4. 设置验证规则 `maxlength="50"` 支持 css 样式与 html 属性

### 效果图

左图为数据不合规时样式，右图为数据合规时样式
![输入图片说明](https://images.gitee.com/uploads/images/2020/0303/170743_43dea689_554725.png "Screen Shot 2020-03-03 at 17.07.29.png")

## 内置验证规则

- required: "这是必填字段"
- remote: "请修正此字段"
- email: "请输入有效的电子邮件地址"
- url: "请输入有效的网址"
- date: "请输入有效的日期"
- dateISO: "请输入有效的日期 (YYYY-MM-DD)"
- number: "请输入有效的数字"
- digits: "只能输入数字"
- creditcard: "请输入有效的信用卡号码"
- equalTo: "你的输入不相同"
- extension: "请输入有效的后缀"
- maxlength: "最多可以输入 {0} 个字符"
- minlength: "最少要输入 {0} 个字符"
- rangelength: "请输入长度在 {0} 到 {1} 之间的字符串"
- range: "请输入范围在 {0} 到 {1} 之间的数值"
- step: "请输入 {0} 的整数倍值"
- max: "请输入不大于 {0} 的数值"
- min: "请输入不小于 {0} 的数值"
- ip: "请填写正确的IP地址"
- radioGroup: "请选择一个选项"
- checkGroup: "请选择一个选项"
- userName: "登录名称不可以包含非法字符"
- greaterThan: "数值必须大于 {0}" 
- lessThan: "数值必须小于 {0}"

## 扩展验证规则

```javascript
$.validator.addMethod("userName", function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9_@.]*$/.test(value);
}, "登录名称不可以包含非法字符");
```

通过静态方法 `$.validator.addMethod` 方法增加自己的验证规则

## Api

获得某个控件的验证组件框架实例 `$('#Id').lgbValidator()`

获得某个验证域内所有组件是否合规 `$('#Id').lgbValid()` 返回 `true` 时表示合规，返回 `false` 时表示不合规

## 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request